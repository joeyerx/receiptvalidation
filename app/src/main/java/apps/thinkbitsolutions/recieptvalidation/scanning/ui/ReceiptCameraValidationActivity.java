package apps.thinkbitsolutions.recieptvalidation.scanning.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.flurgle.camerakit.CameraListener;
import com.flurgle.camerakit.CameraView;

import apps.thinkbitsolutions.recieptvalidation.R;

public class ReceiptCameraValidationActivity extends AppCompatActivity {


    CameraView mCameraView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_camera_validation);

        mCameraView = (CameraView) findViewById(R.id.camera);
        mCameraView.setCameraListener(mCameraListener);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mCameraView.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCameraView.start();
    }

    private CameraListener mCameraListener = new CameraListener() {
        @Override
        public void onCameraOpened() {
            super.onCameraOpened();
        }

        @Override
        public void onCameraClosed() {
            super.onCameraClosed();
        }

        @Override
        public void onPictureTaken(byte[] jpeg) {
            super.onPictureTaken(jpeg);
        }
    };
}
