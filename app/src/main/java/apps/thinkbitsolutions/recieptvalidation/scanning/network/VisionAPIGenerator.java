package apps.thinkbitsolutions.recieptvalidation.scanning.network;

import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by joeyramirez on 28/02/2017.
 */

public class VisionAPIGenerator {


    public static final String API_BASE_URL = "https://vision.googleapis.com/v1";

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    // private static RxJavaCallAdapterFactory rxAdapter = RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
   /*  .addCallAdapterFactory(rxAdapter)*/
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()));

    public static <S> S createService(Class<S> serviceClass) {

        return createService(serviceClass, null, null);
    }

    /*Create service with Auth header*/
    public static <S> S createService(Class<S> serviceClass, String username, String password) {
        if (username != null && password != null) {

            final String basic = Credentials.basic(username, password);

            httpClient.addInterceptor(new Interceptor() {
                @Override
                public Response intercept(Interceptor.Chain chain) throws IOException {
                    Request original = chain.request();

                    Request.Builder requestBuilder = original.newBuilder()
                            .header("Authorization", basic)
                            .header("Accept", "application/json")
                            .method(original.method(), original.body());

                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                }
            });
        }

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = builder.client(client).build();
        return retrofit.create(serviceClass);
    }

}
